var express = require("express");
var app = express();
var path = require("path");
var router = express.Router();
var utenti = require("../Utenti/Utenti.js")

//SERVIAMO SULLA ROOT UN HELLO WORLD
app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname, "..", "Client", "Index.html"));
});

app.get("/listautenti", function(req, res) {
  res.send("questa è la mia lista utenti");
  //Lanciare da un browser "localhost:3000/listautenti" per vedere apparire la scritta "questa è la mia lista utenti"
});

app.get("/films", function(req, res) {
  res.json({"nome":"Rocky", "anno":"2001"});
  //Lanciare da un browser "localhost:3000/films" per vedere apparire l'oggetto creato
});

app.get("/ricerca", function(req, res) {
  res.redirect("http://www.google.com");
  //Lanciare da un browser "localhost:3000/ricerca" per essere reindirizzato a Google
});

app.get("/documento", function(req, res) {
  res.download(path.join(__dirname, "..", "File", "documento.pdf"));
  //Lanciare da un browser "localhost:3000/documento" per fare il download di "documento.pdf" presente nella cartella "File"
});

app.get("/utenti/:id", function(req, res) {
  console.log(req.params.id);
  var id = req.params.id;
  res.send("utente " + id);
  //Lanciare da un browser "localhost:3000/utenti/(esempio:10)"
});

app.get("/youtube", function(req, res) {
  console.log(req.query.titolo);
  var titolo = req.query.titolo;
  res.send("titolo film: " + titolo);
  //Lanciare da un browser "localhost:3000/youtube?titolo=(esempio:gatti)"
});

app.use("/users", utenti);

app.use("/images", express.static(path.join(__dirname, "..", "Client", "Immagini")));

app.listen(3000, function() {
  console.log("server partito su http://localhost:3000");
});
