var express = require("express");
var router = express.Router();

router.get("/", function(req, res) {
  res.send("Lista degli utenti");
});

router.get("/:id", function(req, res) {
  res.send("Dettaglio dell'utente con id: " + req.params.id);
});

module.exports = router;
